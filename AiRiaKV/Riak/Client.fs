﻿namespace AiRiaKV.Riak

open System
open RiakClient


type internal Config =
    { section: string
    }


type internal Client (config: Config) =
    let cluster = RiakCluster.FromConfig(config.section)

    let handle (result: RiakResult) (whenSuccess: unit -> 'a) =
        if result.IsSuccess then
            whenSuccess()
        elif result.Exception <> null then
            raise result.Exception
        else
            sprintf "Got %A with error: %s" result.ResultCode result.ErrorMessage
                |> failwith


    member this.BucketName with get() : string = "AiRiaKV-test"


    member this.Execute (action: IRiakClient -> RiakResult) =
        let client = cluster.CreateClient()
        let result = client |> action
        handle result (fun () -> ())


    member this.Get<'T> (key: string): Option<'T> =
        let client = cluster.CreateClient()
        let result = client.Get(this.BucketName, key)
        (fun () -> result.Value.GetObject<'T>() |> Some)
            |> handle result


    member this.AllKeys (): string list =
        let client = cluster.CreateClient()
        let result = client.ListKeys(this.BucketName)
        (fun () -> result.Value |> List.ofSeq)
            |> handle result


    interface IDisposable with
        member this.Dispose(): unit =
            cluster.Dispose()
