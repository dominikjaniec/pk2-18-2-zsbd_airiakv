﻿namespace AiRiaKV.Riak


module ConnecterFactory =
    open System.Collections.Generic
    open AiRiaKV.Data.Infrastructure
    open RiakClient

    let private keySection = "section-name"

    let private implementationKey =
        "AiRiaKV.Riak.RiakConnecterFactory"


    let private toDict (config: Config) =
        let result = new Dictionary<string, string>(2)
        result.Add(keySection, config.section)
        result

    let private fromDict (dict: IDictionary<string, string>) =
        { section = dict.[keySection]
        }

    let private createSampleStorageConfig () =
        let config = { section = "app.config-section-name" }
        let result = new StorageConfig()
        result.StorageImplementationKey <- implementationKey
        result.Configuration <- toDict config
        result


    type private Connector (config: StorageConfig) =
        let connection = fromDict config.Configuration

        interface IConnecter with
            member this.EnsureValidConfiguration(): unit =
                let ping (riak: IRiakClient) =
                    riak.Ping()

                use client = new Client(connection)
                client.Execute ping


            member this.MakeInserter(): IInserter =
                new Inserter(fun () -> new Client(connection))
                    :> IInserter

            member this.MakeQuerier(): IQuerier =
                new Querier(fun () -> new Client(connection))
                    :> IQuerier


    type RiakConnecterFactory() =
        interface IConnecterFactory with
            member this.ImplementationKey: string =
                implementationKey

            member this.SampleConfig: StorageConfig =
                createSampleStorageConfig ()

            member this.Connecter(config: StorageConfig): IConnecter =
                new Connector(config)
                    :> IConnecter
