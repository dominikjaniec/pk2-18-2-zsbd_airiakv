﻿namespace AiRiaKV.Riak

open System.Diagnostics
open AiRiaKV.Data
open AiRiaKV.Data.Infrastructure

type internal Querier (clientSource: unit -> Client) =
    let sampleCount = 1000

    let everyItem nth items =
        items
            |> Seq.mapi (fun i x -> (i, x))
            |> Seq.filter (fun (i, _) -> i % nth = 0)
            |> Seq.map (fun (_, x) -> x)

    let withStopwatch (action: unit -> 'T) description =
        do printf ">> Stop-watched execution of: %s..." description
        let watch = Stopwatch.StartNew()
        let result = action()
        let elapsed = watch.Elapsed
        printfn " Done within %f seconds." elapsed.TotalSeconds
        result

    let stopwatchedLoadEvery nth keys name (loader: Loader<'T>) =
        let chosens =
            List.filter loader.MatchKey keys
                |> everyItem nth
                |> List.ofSeq

        sprintf "Loading %s of total: %d items" name chosens.Length
            |> withStopwatch (fun () -> loader.LoadEntites chosens)


    let obtainKeys () =
        use client = clientSource()
        let keys =
            "Obtaining all keys at Riak's cluster."
                |> withStopwatch (fun () -> client.AllKeys())

        do printfn ">> >> Got %d keys." keys.Length
        keys


    let loadStations keys =
        new Loader<Station>(clientSource)
            |> stopwatchedLoadEvery 1 keys "every Stations"

    let loadPollutants keys =
        new Loader<Pollutant>(clientSource)
            |> stopwatchedLoadEvery 1 keys "every Pollutants"

    let loadSampleMeasures keys =
        let sampleName = sprintf "sample Measures (every %d-th)" sampleCount
        new Loader<Measure>(clientSource)
            |> stopwatchedLoadEvery sampleCount keys sampleName


    interface IQuerier with
        member this.ExecuteBenchmarkSuit(): uint64 =
            let keys = obtainKeys()
            [ loadStations keys
            ; loadPollutants keys
            ; loadSampleMeasures keys
            ] |> List.sum

        member this.Dispose(): unit =
            ()
