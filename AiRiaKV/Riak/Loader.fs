﻿namespace AiRiaKV.Riak

open AiRiaKV.Data.Transciency

type internal Loader<'T> (clientProvider: unit -> Client) =
    let chunkSize = 10000
    let keyPrefix = KeyMaker.PrefixOf<'T>()

    let loadEntity (client: Client) key =
        match client.Get<'T>(key) with
        | Some _ -> ()
        | None ->
            sprintf "Expecting to got %s of key: '%s'." typeof<'T>.Name key
                |> failwith


    member this.MatchKey (key: string) =
        key.StartsWith(keyPrefix)


    member this.LoadEntites keys =
        let loadNext (client: Client) n key =
            do loadEntity client key
            n + 1L

        let loadBatch batch =
            use client = clientProvider()
            let folder = loadNext client
            let loaded = Seq.fold folder 0L batch

            do printf "."
            loaded

        Seq.chunkBySize chunkSize keys
            |> Seq.map loadBatch
            |> Seq.sum
            |> uint64
