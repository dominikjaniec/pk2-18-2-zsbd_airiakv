﻿namespace AiRiaKV.Riak

open System.Collections.Generic
open AiRiaKV.Data
open AiRiaKV.Data.Infrastructure
open AiRiaKV.Data.Transciency
open RiakClient
open RiakClient.Models


type internal Inserter (clientProvider: unit -> Client) =
    let chunkSize = 1000

    let insertEntites (keyer: 'T -> string) (entities: 'T seq) =
        let insertNext (client: Client) n item =
            let key = keyer item
            let bucket = client.BucketName
            let obj = new RiakObject(bucket, key, item)
            do client.Execute (fun riak -> riak.Put(obj) :> RiakResult)
            n + 1L

        let insertBatch batch =
            use client = clientProvider()
            let folder = insertNext client
            let inserted =
                    Seq.fold folder 0L batch

            do printf "."
            inserted

        Seq.chunkBySize chunkSize entities
            |> Seq.map insertBatch
            |> Seq.sum
            |> uint64


    let insertCodedEntities entities =
        let keyOf entity = KeyMaker.Of<'T>(entity)
        insertEntites keyOf entities


    let insertMeasures measures =
        let keyOf (measure: Measure) = KeyMaker.Of(measure)
        insertEntites keyOf measures


    interface IInserter with
        member this.InsertStations(stations: IEnumerable<Station>): uint64 =
            stations
                |> insertCodedEntities

        member this.InsertPollutants(pollutants: IEnumerable<Pollutant>): uint64 =
            pollutants
                |> insertCodedEntities

        member this.InsertMeasures(measures: IEnumerable<Measure>): uint64 =
            measures
                |> insertMeasures

        member this.Dispose(): unit =
            ()
