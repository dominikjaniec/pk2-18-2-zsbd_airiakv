﻿using System.Collections.Generic;

namespace AiRiaKV.Data.Transciency
{
    public class SourcesPaths
    {
        public string StationsMetadata { get; set; }
        public string PollutantsMetadata { get; set; }
        public IDictionary<string, string[]> PollutantsMeasures { get; set; }
    }
}
