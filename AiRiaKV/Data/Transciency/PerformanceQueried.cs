﻿using System;

namespace AiRiaKV.Data.Transciency
{
    public class PerformanceQueried
    {
        public string CaseDescription { get; set; }
        public DateTimeOffset CaseTimestamp { get; set; }
        public ulong BenchmarkSuitOperations { get; set; }
        public TimeSpan BenchmarkSuitTime { get; set; }
    }
}
