﻿namespace AiRiaKV.Data.Transciency
{
    public static class KeyMaker
    {
        public static string PrefixOf<T>()
            => "/" + typeof(T).Name.ToLowerInvariant();

        public static string Of<TCoded>(TCoded coded)
            where TCoded : ICoded
            => PrefixOf<TCoded>() + coded.Code + "/";

        public static string Of(Measure measure)
            => PrefixOf<Measure>() + $"{measure.PollutantCode}/{measure.StationCode}/{measure.Date.ToString("yyyymmdd")}/";
    }
}
