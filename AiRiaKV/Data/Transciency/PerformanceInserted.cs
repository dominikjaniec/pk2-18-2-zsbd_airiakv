﻿using System;

namespace AiRiaKV.Data.Transciency
{
    public class PerformanceInserted
    {
        public string CaseDescription { get; set; }
        public DateTimeOffset CaseTimestamp { get; set; }
        public ulong StationsInsertionOperations { get; set; }
        public ulong PollutantsInsertionOperations { get; set; }
        public ulong MeasuresInsertionOperations { get; set; }
        public TimeSpan PollutantsInsertionTime { get; set; }
        public TimeSpan StationsInsertionTime { get; set; }
        public TimeSpan MeasuresInsertionTime { get; set; }
    }
}
