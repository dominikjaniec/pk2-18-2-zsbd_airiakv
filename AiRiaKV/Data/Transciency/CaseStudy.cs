﻿using System;
using System.Collections.Generic;

namespace AiRiaKV.Data.Transciency
{
    public class CaseStudy
    {
        public string CaseDescription { get; set; }
        public DateTimeOffset CaseTimestamp { get; set; }

        public ulong TotalStations { get; set; }
        public IEnumerable<Station> Stations { get; set; }

        public ulong TotalPollutants { get; set; }
        public IEnumerable<Pollutant> Pollutants { get; set; }
    }
}
