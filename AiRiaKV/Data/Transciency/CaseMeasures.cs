﻿using System.Collections.Generic;

namespace AiRiaKV.Data.Transciency
{
    public class CaseMeasures
    {
        public string PollutantCode { get; set; }
        public ulong TotalMeasures { get; set; }
        public IEnumerable<Measure> Measures { get; set; }
    }
}
