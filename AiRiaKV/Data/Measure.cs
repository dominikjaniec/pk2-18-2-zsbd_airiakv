﻿using System;
using System.Collections.Generic;

namespace AiRiaKV.Data
{
    public class Measure
    {
        public DateTime Date { get; set; }
        public TimeSpan Resolution { get; set; }
        public string StationCode { get; set; }
        public string PollutantCode { get; set; }
        public List<decimal?> Values { get; set; }
    }
}
