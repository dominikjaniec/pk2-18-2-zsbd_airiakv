﻿namespace AiRiaKV.Data
{
    public class Station : ICoded
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
    }
}
