﻿namespace AiRiaKV.Data
{
    public class Pollutant : ICoded
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public string MeasureResolution { get; set; }
    }
}
