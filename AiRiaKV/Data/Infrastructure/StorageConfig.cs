﻿using System.Collections.Generic;

namespace AiRiaKV.Data.Infrastructure
{
    public class StorageConfig
    {
        public string StorageImplementationKey { get; set; }
        public IDictionary<string, string> Configuration { get; set; }
    }
}
