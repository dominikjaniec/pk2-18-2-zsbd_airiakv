﻿using System;
using System.Collections.Generic;

namespace AiRiaKV.Data.Infrastructure
{
    public interface IInserter : IDisposable
    {
        /// <summary>
        /// Inserts all given instances of <see cref="Station"/>.
        /// </summary>
        /// <param name="stations"></param>
        /// <returns>Count of executed operations.</returns>
        ulong InsertStations(IEnumerable<Station> stations);

        /// <summary>
        /// Inserts all given instances of <see cref="Pollutant"/>.
        /// </summary>
        /// <param name="pollutants"></param>
        /// <returns>Count of executed operations.</returns>
        ulong InsertPollutants(IEnumerable<Pollutant> pollutants);

        /// <summary>
        /// Inserts all given instances of <see cref="Measure"/>.
        /// </summary>
        /// <param name="measures"></param>
        /// <returns>Count of executed operations.</returns>
        ulong InsertMeasures(IEnumerable<Measure> measures);
    }
}
