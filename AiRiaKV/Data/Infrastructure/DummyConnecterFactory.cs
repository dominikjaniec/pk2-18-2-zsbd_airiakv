﻿using System;
using System.Collections.Generic;

namespace AiRiaKV.Data.Infrastructure
{
    public class DummyConnecterFactory : IConnecterFactory
    {
        public string ImplementationKey { get; }
            = typeof(DummyConnecterFactory).FullName;

        public StorageConfig SampleConfig
            => new StorageConfig
            {
                StorageImplementationKey = ImplementationKey,
                Configuration = new Dictionary<string, string>
                {
                    ["this-field"] = "ignored",
                    ["other-key"] = "some value"
                }
            };

        public IConnecter Connecter(StorageConfig config)
            => new DummyConnecter();

        private class DummyConnecter : IConnecter
        {
            public void EnsureValidConfiguration() { }
            public IInserter MakeInserter() => new Inserter();
            public IQuerier MakeQuerier() => new Querier();
        }

        private class Inserter : DummyDisposable, IInserter
        {
            public ulong InsertStations(IEnumerable<Station> stations) => 7;
            public ulong InsertPollutants(IEnumerable<Pollutant> pollutants) => 13;
            public ulong InsertMeasures(IEnumerable<Measure> measures) => 42;
        }

        private class Querier : DummyDisposable, IQuerier
        {
            public ulong ExecuteBenchmarkSuit() => 1691;
        }

        private abstract class DummyDisposable : IDisposable
        {
            public void Dispose() { }
        }
    }
}
