﻿namespace AiRiaKV.Data.Infrastructure
{
    public interface IConnecterFactory
    {
        /// <summary>
        /// Provides supported <see cref="StorageConfig.StorageImplementationKey"/>.
        /// </summary>
        string ImplementationKey { get; }

        /// <summary>
        /// Represents sample <see cref="IConnecter"/>'s configuration.
        /// </summary>
        StorageConfig SampleConfig { get; }

        /// <summary>
        /// Obtains <see cref="IConnecter"/> based on given <see cref="StorageConfig"/>.
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        IConnecter Connecter(StorageConfig config);
    }
}
