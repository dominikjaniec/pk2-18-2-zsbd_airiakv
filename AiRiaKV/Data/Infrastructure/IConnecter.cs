﻿namespace AiRiaKV.Data.Infrastructure
{
    public interface IConnecter
    {
        /// <summary>
        /// Ensures that given connect's configuration of connection to the Storage is working.
        /// Should throw <see cref="System.InvalidOperationException"/> in other case.
        /// </summary>
        void EnsureValidConfiguration();

        /// <summary>
        /// Prepares <see cref="IInserter"/> for given Storage system.
        /// </summary>
        /// <returns></returns>
        IInserter MakeInserter();

        /// <summary>
        /// Prepares <see cref="IQuerier"/> for given Storage system.
        /// </summary>
        /// <returns></returns>
        IQuerier MakeQuerier();
    }
}
