﻿using System;

namespace AiRiaKV.Data.Infrastructure
{
    public interface IQuerier : IDisposable
    {
        /// <summary>
        /// Executes suit of benchmark queries for given Storage system.
        /// </summary>
        /// <returns>Count of executed operations.</returns>
        ulong ExecuteBenchmarkSuit();
    }
}
