﻿namespace AiRiaKV.Data
{
    public interface ICoded
    {
        string Code { get; }
        string FullName { get; }
    }
}
