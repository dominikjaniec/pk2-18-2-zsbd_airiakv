﻿namespace AiRiaKV.Console

module Sampler =
    open System
    open System.IO
    open System.Linq
    open System.Text.RegularExpressions
    open AiRiaKV.Air
    open AiRiaKV.Data.Infrastructure
    open AiRiaKV.Data.Transciency


    let private showJson obj (verb, file) =
        printfn "======================================================================="
        printfn "=== The %s's JSON file: '%s'" verb file
        printfn "==="
        printfn "%s" (Stringifier.toJson obj)
        printfn "======================================================================="


    let private sampleTransformSourceShowJson pollutantsMetadta pollutants =
        let adjust (code, paths) =
            (code, Array.ofList paths)

        let source = new SourcesPaths()
        source.StationsMetadata <- "GIOŚ-stations_metadata.xlsx"
        source.PollutantsMetadata <- Cli.makePossiblyRelative pollutantsMetadta
        source.PollutantsMeasures <- List.map adjust pollutants |> dict
        (Cli.transform, Cli.transformSource)
            |> showJson source
        0


    let private classifyMeasuresAs pollutants files =
        let fileCode = new Regex("\d{4}_(?<code>[^_]+)_.*")
        let notNormalized = new Regex("[^a-z0-9]")

        let asNormalized (code: string) =
            (notNormalized.Replace(code.ToLowerInvariant(), ""), code)

        let caseInvariantOrder x y =
            StringComparer.InvariantCultureIgnoreCase.Compare(x, y)

        let caseInvariantOrderOf get x y =
            caseInvariantOrder (get x) (get y)

        let pollutantsCodes =
            Set.ofSeq pollutants

        let pollutatntsCodesByNormalized =
            let asResult (normalized, group) =
                let codes =
                    Seq.map snd group
                        |> List.ofSeq

                (normalized, codes)

            pollutantsCodes
                |> Seq.map asNormalized
                |> Seq.groupBy fst
                |> Seq.map asResult
                |> dict

        let extractCodeFromFile path =
            let fileCodeMatch =
                Path.GetFileNameWithoutExtension(path)
                    |> fileCode.Match

            let extractedCode =
                match fileCodeMatch.Success with
                | true -> Some fileCodeMatch.Groups.["code"].Value
                | false -> None

            extractedCode
                |> Option.map asNormalized

        let classify filePath =
            let resolve (normalized, code) =
                match pollutatntsCodesByNormalized.ContainsKey normalized with
                | true -> pollutatntsCodesByNormalized.[normalized]
                | false -> [ sprintf "[unknown-code] %s" code ]

            let codes =
                extractCodeFromFile filePath
                    |> Option.map resolve
                    |> Option.defaultValue []

            (filePath, codes)

        let classifications =
            Seq.map classify files
                |> List.ofSeq

        let directChooser (path, codes) =
            match List.length codes with
            | 1 -> Some (List.head codes, path)
            | _ -> None

        let undecidedChooser (path, codes) =
            match List.length codes > 1 with
            | true -> Some ("[undecided-code]", path)
            | false -> None

        let notFoundChooser (path, codes) =
            match List.isEmpty codes with
            | true -> Some ("[code-not-found]", path)
            | false -> None

        let matchesBy chooser =
            let asResult (code, matches) =
                let files =
                    matches
                        |> Seq.map snd
                        |> Seq.sortWith caseInvariantOrder
                        |> List.ofSeq

                (code, files)

            classifications
                |> Seq.choose chooser
                |> Seq.groupBy fst
                |> Seq.map asResult
                |> Seq.sortWith (caseInvariantOrderOf fst)
                |> List.ofSeq

        let missingMatches direct =
            let directMatchCodes =
                List.map fst direct

            let missing code =
                directMatchCodes
                    |> List.contains code
                    |> not

            pollutantsCodes
                |> Seq.filter missing
                |> Seq.sortWith caseInvariantOrder
                |> Seq.map (fun c -> (c, []))
                |> List.ofSeq

        let asDirectMatch = matchesBy directChooser
        let asMissingMatch = missingMatches asDirectMatch

        [ matchesBy notFoundChooser
        ; matchesBy undecidedChooser
        ; asDirectMatch
        ; asMissingMatch
        ] |> List.concat


    let private loadPollutantsMeasuresFiles statisticsFile measuresDirectory =
        let pollutantsCodes =
            Cli.resolveFullPath statisticsFile
                |> PollutantsLoader.loadPollutants
                |> List.map fst

        Cli.resolveFullPath measuresDirectory
            |> Directory.GetFiles
            |> Seq.map Cli.makePossiblyRelative
            |> classifyMeasuresAs pollutantsCodes


    let sampleInsertQueryStorageConfigShowJsonAs verb =
        let configuration =
            [ ("sample-key", "sample value")
            ; ("other", "this-is-related-to-given-implementation")
            ] |> dict

        let storageConfig = new StorageConfig();
        storageConfig.StorageImplementationKey <- "IMPLEMENTATION_KEY"
        storageConfig.Configuration <- configuration
        (verb, Cli.insertConfig)
            |> showJson storageConfig
        0


    let action args =
        match List.tail args with
        | [ Cli.transform ] ->
            [ ("O2", [ "sample-O2.xslx"; "magic.csv" ])
            ; ("H(PM9000)", [ "2049-H_sample.xslx" ])
            ] |> sampleTransformSourceShowJson "GIOŚ-pollutants_statistics.xlsx"

        | [ Cli.transform; statistics; measures ] ->
            loadPollutantsMeasuresFiles statistics measures
                |> sampleTransformSourceShowJson (Cli.resolveFullPath statistics)

        | [ Cli.insert; Cli.sampleInsertQueryStorageConfig ] ->
            sampleInsertQueryStorageConfigShowJsonAs Cli.insert

        | [ Cli.query; Cli.sampleInsertQueryStorageConfig ] ->
            sampleInsertQueryStorageConfigShowJsonAs Cli.query

        | _ -> Cli.showHelpPage args
