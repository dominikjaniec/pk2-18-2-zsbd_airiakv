﻿namespace AiRiaKV.Air

module Stringifier =
    open System.Diagnostics
    open System.IO
    open Newtonsoft.Json


    let private stopwatched<'T> (action: unit -> 'T) =
        let watch = Stopwatch.StartNew()
        let result = action ()
        let elapsed = watch.Elapsed
        (result, elapsed)


    let private toJsonMeasured<'T> (entity: 'T) =
        (fun () -> JsonConvert.SerializeObject(entity, Formatting.Indented))
            |> stopwatched


    let private fromJsonMeasured<'T> (json: string) =
        (fun () -> JsonConvert.DeserializeObject<'T>(json))
            |> stopwatched


    let toJson<'T> (entity: 'T) =
        toJsonMeasured entity
            |> fst


    let toJsonFile<'T> (filePath: string) (entity: 'T) =
        printf ">> Serializing '%s' to file: `%s`..." typeof<'T>.FullName filePath
        let serialized = toJsonMeasured entity
        let saved = stopwatched (fun () -> File.WriteAllText(filePath, fst serialized))
        printfn " Serialized within %f seconds, saved within %f seconds." (snd serialized).TotalSeconds (snd saved).TotalSeconds


    let fromJson<'T> (json: string) =
        fromJsonMeasured<'T> json
            |> fst


    let fromJsonFile<'T> (filePath: string) =
        printf ">> Deserializing '%s' from file: `%s`..." typeof<'T>.FullName filePath
        let loaded = stopwatched (fun () -> File.ReadAllText(filePath))
        let deserialized = fromJsonMeasured<'T> (fst loaded)

        printfn " Loaded within %f seconds, deserialized within %f seconds." (snd loaded).TotalSeconds (snd deserialized).TotalSeconds
        fst deserialized
