﻿namespace AiRiaKV.Console

module Transformer =
    open AiRiaKV.Air
    open AiRiaKV.Data
    open AiRiaKV.Data.Transciency
    open System
    open System.Collections.Generic


    let private makeCaseStudy (pollutants: IDictionary<string, Pollutant>) (stations: IDictionary<string, Station>) =
        let caseStudy = new CaseStudy()
        caseStudy.CaseTimestamp <- DateTimeOffset.Now
        caseStudy.CaseDescription <- "Loaded with fight"
        caseStudy.Stations <- stations.Values |> Array.ofSeq
        caseStudy.Pollutants <- pollutants.Values |> Array.ofSeq
        caseStudy


    let private makeCaseMeasuresFor (pollutant: Pollutant) (measures: Measure seq) =
        let caseMeasures = new CaseMeasures()
        caseMeasures.PollutantCode <- pollutant.Code
        caseMeasures.Measures <- measures |> Array.ofSeq
        caseMeasures


    let measuresPollutantFile (resultFile: string) (pollutant: Pollutant) =
            // TODO : Do not repeat `json` in name.
            sprintf "%s.%s.json" resultFile pollutant.Code


    let execute source result =
        let sources = Stringifier.fromJsonFile<SourcesPaths> source
        let totalBatches = sources.PollutantsMeasures.Keys.Count
        let pollutants = PollutantsLoader.asDict sources
        let stations = StationsLoader.asDict sources
        printfn ">> Loaded %d pollutants and %d stations." pollutants.Keys.Count stations.Keys.Count

        let processMeasuresFiles (i: int) (code: string, measuresFiles: string seq) =
            let pollutant = pollutants.[code]
            let resultFile = measuresPollutantFile result pollutant
            printfn ">> Loading measures of pollutant: '%s' [%d/%d]..." pollutant.Code (i + 1) totalBatches
            measuresFiles
                |> MeasuresLoader.loadFor pollutant stations
                |> makeCaseMeasuresFor pollutant
                |> Stringifier.toJsonFile resultFile

        makeCaseStudy pollutants stations
            |> Stringifier.toJsonFile result

        sources.PollutantsMeasures
            |> Seq.map (fun kvp -> (kvp.Key, kvp.Value))
            |> Seq.iteri processMeasuresFiles


    let action args =
        match args with
        | [ source; result ] -> execute source result
        | [ source ] -> execute source Cli.transformResult
        | _ -> execute Cli.transformSource Cli.transformResult
