﻿namespace AiRiaKV.Console

module Inserter =
    open System
    open System.Diagnostics
    open System.IO
    open System.Threading
    open AiRiaKV.Air
    open AiRiaKV.Data.Infrastructure
    open AiRiaKV.Data.Transciency


    let private stopwatchedInsertion (inserter: IInserter) (caseStudy: CaseStudy) (measuresSources: (unit -> CaseMeasures) seq) =
        let withStopwatchAndRetry (action: unit -> uint64) description =
            let mutable result = (0UL, TimeSpan.Zero)
            let mutable keepTrying = true
            printf ">> Stop-watched execution of: %s..." description
            while keepTrying do
                try
                    let watch = Stopwatch.StartNew()
                    let operations = action()
                    let elapsed = watch.Elapsed
                    printfn " Done %d operations within %f seconds." operations elapsed.TotalSeconds
                    result <- (operations, elapsed)
                    keepTrying <- false
                with
                    | ex ->
                        let waitSeconds = 30
                        printfn "!!! Got %s: '%s', retry after %d seconds..." (ex.GetType().FullName) ex.Message waitSeconds
                        Thread.Sleep(TimeSpan.FromSeconds(waitSeconds |> float))
            result

        let merge (ops: uint64, elapsed: TimeSpan) (nextOps: uint64, nextElapsed: TimeSpan) =
            (ops + nextOps, elapsed.Add(nextElapsed))

        let insertedStations =
            sprintf "Inserting batch of %d stations" caseStudy.TotalStations
                |> withStopwatchAndRetry (fun () -> inserter.InsertStations(caseStudy.Stations))

        let insertedPollutants =
            sprintf "Inserting batch of %d pollutants" caseStudy.TotalPollutants
                |> withStopwatchAndRetry (fun () -> inserter.InsertPollutants(caseStudy.Pollutants))

        let insertedCaseMeasures =
            let insertNextBatch (ops: uint64, elapsed: TimeSpan) (caseMeasuresSource: (unit -> CaseMeasures)) =
                let caseMeasures = caseMeasuresSource()
                sprintf "Inserting batch of %d measures for: '%s' pollutant" caseMeasures.TotalMeasures caseMeasures.PollutantCode
                    |> withStopwatchAndRetry (fun () -> inserter.InsertMeasures(caseMeasures.Measures))
                    |> merge (ops, elapsed)
            measuresSources
                |> Seq.fold insertNextBatch (0UL, TimeSpan.Zero)

        let result = new PerformanceInserted()
        result.CaseDescription <- caseStudy.CaseDescription
        result.CaseTimestamp <- caseStudy.CaseTimestamp
        result.StationsInsertionOperations <- fst insertedStations
        result.StationsInsertionTime <- snd insertedStations
        result.PollutantsInsertionOperations <- fst insertedPollutants
        result.PollutantsInsertionTime <- snd insertedPollutants
        result.MeasuresInsertionOperations <- fst insertedCaseMeasures
        result.MeasuresInsertionTime <- snd insertedCaseMeasures

        let (totalOps, totalElapsed) = insertedStations |> merge insertedPollutants |> merge insertedCaseMeasures
        printfn ">> Total insertions statistics: %d operations within %f seconds" totalOps totalElapsed.TotalSeconds
        result


    let execute configFile caseStudyFile resultFile =
        let onlyExistingFiles filePath =
            let exists = File.Exists(filePath)
            if not exists then
                printfn ">> Could not find file: '%s', skipping it." filePath
            exists

        let makeMeasuresSource filePath =
            (fun () -> Stringifier.fromJsonFile<CaseMeasures> filePath)

        let storageConfig = Stringifier.fromJsonFile<StorageConfig> configFile
        let caseStudy = Stringifier.fromJsonFile<CaseStudy> caseStudyFile

        let measuresSources =
            caseStudy.Pollutants
                |> Seq.map (Transformer.measuresPollutantFile caseStudyFile)
                |> Seq.filter onlyExistingFiles
                |> Seq.map makeMeasuresSource

        let connecter =
            (Storage.getConnecterFactory storageConfig)
                .Connecter(storageConfig)

        printfn ">> Inserting Air-Data with performance measurement from: CaseStudy at %A: '%s'." caseStudy.CaseTimestamp caseStudy.CaseDescription

        connecter.EnsureValidConfiguration()
        use inserter = connecter.MakeInserter()
        stopwatchedInsertion inserter caseStudy measuresSources
            |> Stringifier.toJsonFile resultFile


    let action args =
        match args with
        | [ config; source; result ] -> execute config source result
        | [ config; source ] -> execute config source Cli.insertResult
        | [ config ] -> execute config Cli.insertSource Cli.insertResult
        | _ -> execute Cli.insertConfig Cli.insertSource Cli.insertResult
