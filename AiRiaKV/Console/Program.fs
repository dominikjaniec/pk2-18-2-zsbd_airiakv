﻿namespace AiRiaKV.Console

module Program =
    let private standard name args action =
        let resolvedArgs =
            List.tail args
                |> List.map Cli.resolveFullPath

        let cliAction args =
            action args
            0

        (name, resolvedArgs, cliAction)


    let private resolveAction args =
        let lower (s: string) =
            s.ToLower()

        let requestedAction =
            List.tryHead args
                |> Option.map lower

        match requestedAction with
        | Some Cli.transform -> standard Cli.transform args Transformer.action
        | Some Cli.insert -> standard Cli.insert args Inserter.action
        | Some Cli.query -> standard Cli.query args Querier.action
        | Some Cli.sample -> (Cli.sample, args, Sampler.action)
        | _ -> (Cli.help, args, Cli.showHelpPage)


    let private callAction (name, args, action) =
        printfn "############ AiRiaKV : %s ############ by Dominik Janiec, 2018 KRK." name
        action args


    [<EntryPoint>]
    let main argv =
        Array.toList argv
            |> resolveAction
            |> callAction
