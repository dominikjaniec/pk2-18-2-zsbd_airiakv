﻿namespace AiRiaKV.Console

module Cli =
    open System.IO

    [<Literal>]
    let transform = "transform"
    let transformSource = "sources-config.json"
    let transformResult = "data-transformed.json"

    [<Literal>]
    let insert = "insert"
    let insertConfig = "storage-config.json"
    let insertSource = transformResult
    let insertResult = "performance-inserted.json"

    [<Literal>]
    let query = "query"
    let queryConfig = insertConfig
    let queryResult = "performance-queried.json"

    [<Literal>]
    let sample = "sample"
    [<Literal>]
    let sampleTransformPrepareArgs = "'pollutants_statistics.xlsx' 'measures_directory'"
    [<Literal>]
    let sampleInsertQueryStorageConfig = "storage-config"

    [<Literal>]
    let help = "help"
    let showHelpPage args =
        printfn "Invalid arguments: %A" args
        printfn ""
        printfn "Please execute with one of verbs:"
        printfn "  * %s [%s [%s]]" transform transformSource transformResult
        printfn "  * %s [%s [%s [%s]]]" insert insertConfig insertSource insertResult
        printfn "  * %s [%s [%s]]" query queryConfig queryResult
        printfn ""
        printfn "To obtain sample JSONs files for presented verbs above:"
        printfn "  * %s %s [%s]" sample transform sampleTransformPrepareArgs
        printfn "  * %s %s %s" sample insert sampleInsertQueryStorageConfig
        printfn "  * %s %s %s" sample query sampleInsertQueryStorageConfig
        13


    let resolveFullPath (givenPath: string) =
        let mutable path = givenPath
        path <- Path.Combine(Directory.GetCurrentDirectory(), path)
        path <- Path.GetFullPath(path)
        path


    let makePossiblyRelative (givenPath: string) =
        let workingDirectory =
            Directory.GetCurrentDirectory() + "\\"

        match givenPath.StartsWith(workingDirectory) with
        | true -> givenPath.Substring(workingDirectory.Length )
        | _ -> givenPath
