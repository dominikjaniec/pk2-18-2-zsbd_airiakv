﻿namespace AiRiaKV.Console

module Storage =
    open AiRiaKV.Data.Infrastructure
    open AiRiaKV.Riak.ConnecterFactory

    let private implementations =
        [ new DummyConnecterFactory() :> IConnecterFactory
        ; new RiakConnecterFactory() :> IConnecterFactory
        //; new PostgresConnecterFactory() :> IConnecterFactory
        //; new RedisConnecterFactory() :> IConnecterFactory
        ] |> Seq.map (fun imp -> (imp.ImplementationKey, imp))
            |> dict

    let getConnecterFactory (config: StorageConfig) =
        let key = config.StorageImplementationKey
        match implementations.TryGetValue key with
        | (true, implementation) ->
            printfn ">> Found Storage implementation under key: '%s'." key
            implementation

        | _ ->
            sprintf "Unknown Storage implementations: %s." key
                |> failwith
