﻿namespace AiRiaKV.Console

module Querier =
    open AiRiaKV.Air
    open AiRiaKV.Data.Infrastructure
    open System.Diagnostics
    open AiRiaKV.Data.Transciency
    open System

    let private stopwatchedBenchmark (action: unit -> uint64) =
        let watch = Stopwatch.StartNew()
        let operations = action()
        let elapsed = watch.Elapsed
        (operations, elapsed)

    let private toPerformanceReport (operations, elapsed) =
        let report = new PerformanceQueried()
        report.CaseDescription <- "Unknown Magic - NOT IMPLEMENTED"
        report.CaseTimestamp <- DateTimeOffset.Now
        report.BenchmarkSuitOperations <- operations
        report.BenchmarkSuitTime <- elapsed
        report

    let execute configFile resultFile =
        let storageConfig = Stringifier.fromJsonFile<StorageConfig> configFile
        let connecter =
            (Storage.getConnecterFactory storageConfig)
                .Connecter(storageConfig)

        printfn ">> Querying Air-Data with performance measurement..."

        connecter.EnsureValidConfiguration()
        use querier = connecter.MakeQuerier()
        (fun () -> querier.ExecuteBenchmarkSuit() )
            |> stopwatchedBenchmark
            |> toPerformanceReport
            |> Stringifier.toJsonFile resultFile


    let action args =
        match args with
        | [ config; result ] -> execute config result
        | [ config ] -> execute config Cli.queryResult
        | _ -> execute Cli.queryConfig Cli.queryResult
