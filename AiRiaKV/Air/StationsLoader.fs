﻿namespace AiRiaKV.Air

module StationsLoader =
    open System.IO
    open AiRiaKV.Data
    open AiRiaKV.Data.Transciency
    open ExcelDataReader


    let private getStationAddress (reader: IExcelDataReader) =
        let town = reader.GetString(12)
        let street = reader.GetString(13)
        let province = reader.GetString(11)
        let n = reader.GetDouble(14)
        let e = reader.GetDouble(15)

        sprintf "%s %s | %s (N:%f E:%f)" town street province n e


    let private getStation (i: int) (reader: IExcelDataReader) =
        if reader.IsDBNull(1) then
            failwithf "Station at %dth row does not have current Stations' Code." (i + 1)

        let station = new Station()
        station.Code <- reader.GetString(1)
        station.FullName <- reader.GetString(3)
        station.Address <- getStationAddress reader

        [station.Code; reader.GetString(2); reader.GetString(4)]
            |> Seq.filter (fun c -> c <> null)
            |> Seq.map (fun c -> (c, station))
            |> List.ofSeq


    let private loadStations path =
        let mutable stations = []
        let mutable i = 1

        use stream = File.Open(path, FileMode.Open, FileAccess.Read)
        use excelReader = ExcelReaderFactory.CreateReader(stream)
        excelReader.Read()
            |> ignore // Ignore Header row.

        while excelReader.Read() do
            let newStation = getStation i excelReader
            stations <- newStation :: stations
            i <- i + 1

        stations


    let asDict (sources: SourcesPaths) =
        printfn ">> Loading Stations' data from file: `%s`..." sources.StationsMetadata
        let stations = loadStations sources.StationsMetadata

        let result = stations |> List.concat |> dict
        printfn "\t Got %d Stations with %d unique codes." (List.length stations) result.Keys.Count
        printfn ""

        result
