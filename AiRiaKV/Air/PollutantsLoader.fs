﻿namespace AiRiaKV.Air

module PollutantsLoader =
    open System.IO
    open AiRiaKV.Data
    open AiRiaKV.Data.Transciency
    open ExcelDataReader


    let private getPollutant code=
        let pollutant = new Pollutant()
        pollutant.Code <- code
        pollutant.FullName <- sprintf "Pollutant: %s" code
        pollutant.MeasureResolution <- "ug/m3"
        pollutant


    let private extractOther (excelReader: IExcelDataReader) =
        excelReader.Read()
            |> ignore // Ignore Header row.

        let mutable pollutants = []
        while excelReader.Read() do
            pollutants <- excelReader.GetString(6) :: pollutants

        pollutants
            |> List.distinct


    let loadPollutants path =
        let mutable pollutants = []
        let mutable hasSheets = true

        use stream = File.Open(path, FileMode.Open, FileAccess.Read)
        use excelReader = ExcelReaderFactory.CreateReader(stream)

        while hasSheets do
            match excelReader.Name with
            | "Inne" -> pollutants <- List.concat [extractOther excelReader; pollutants]
            | sheetName -> pollutants <- sheetName :: pollutants
            hasSheets <- excelReader.NextResult()

        pollutants
            |> Seq.map getPollutant
            |> Seq.map (fun p -> (p.Code, p))
            |> List.ofSeq


    let asDict (sources: SourcesPaths) =
        printfn ">> Loading Pollutants' data from file: `%s`..." sources.PollutantsMetadata
        let pollutants = loadPollutants sources.PollutantsMetadata

        let result = dict pollutants
        printfn "\t Got %d Pollutants." (List.length pollutants)
        printfn ""

        result
