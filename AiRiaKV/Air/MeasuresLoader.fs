﻿namespace AiRiaKV.Air

module MeasuresLoader =
    open System
    open System.Collections.Generic
    open System.IO
    open AiRiaKV.Data
    open ExcelDataReader


    let private failByUnexpectedFileSchema message =
        failwith (sprintf "Unexpected file schema. %s" message)


    let private extractStations (excelReader: IExcelDataReader) (stations: IDictionary<string, Station>) =
        let toStationsRow () =
            if not (excelReader.Read()) then
                failByUnexpectedFileSchema "Expecting some data."

            while excelReader.GetFieldType(1) <> typeof<String> do
                if not (excelReader.Read()) then
                    failByUnexpectedFileSchema "Could not find Stations' row."

        let getStationAt (idx: int) =
            let createUnknown code =
                printf " ### Unknown stations' code: '%s' at %dth column ###" code (idx + 1)
                let station = new Station()
                station.Code <- code
                station.FullName <- sprintf "[unknown] %s" code
                station.Address <- sprintf "Unknown station: %s" code
                station

            let stationCode = excelReader.GetString(idx)
            match stations.TryGetValue stationCode with
            | (true, station) -> station
            | _ -> createUnknown stationCode

        let getStations () =
            let findLastStationIndex () =
                let mutable potential = excelReader.FieldCount - 1
                while excelReader.IsDBNull(potential) do
                    potential <- potential - 1

                potential

            seq { 1 .. findLastStationIndex () }
                |> Seq.map getStationAt
                |> List.ofSeq

        toStationsRow ()
        getStations ()


    let private getMeasures (excelReader: IExcelDataReader) (pollutant: Pollutant) (stations: Station list) =
        let toMeasuresRows () =
            while excelReader.GetFieldType(0) <> typeof<DateTime> do
               if not (excelReader.Read()) then
                    failByUnexpectedFileSchema "Could not find Measurements' rows."

        let extractMeasureValue (i: int) (station: Station) =
            let value =
                match excelReader.GetValue(i + 1) with
                | null -> Nullable<decimal>()
                | v -> Nullable<decimal>(Convert.ToDecimal(v))

            (station, value)

        let makeMeasures (date: DateTime) =
            let create ((station: Station), (value: Nullable<decimal>)) =
                let values = new List<Nullable<decimal>>()
                values.Add(value)

                let measure = new Measure()
                measure.Date <- date
                measure.Resolution <- TimeSpan.Zero
                measure.StationCode <- station.Code
                measure.PollutantCode <- pollutant.Code
                measure.Values <- values
                measure

            stations
                |> Seq.mapi extractMeasureValue
                |> Seq.map create
                |> List.ofSeq

        let addMeasuresValues (batch: Measure list) =
            let update (i: int) (_: Station, value: Nullable<decimal>) =
                batch.[i].Values.Add(value)

            stations
                |> Seq.mapi extractMeasureValue
                |> Seq.iteri update

        let measuresCount (measures: Measure list list) =
            measures
                |> Seq.concat
                |> Seq.sumBy (fun m -> m.Values.Count)

        toMeasuresRows ()

        let mutable measures = []
        let mutable batch = []
        let mutable lastDate = DateTime.MinValue
        while excelReader.Read() do
            if not (excelReader.IsDBNull(0)) then
                let date = excelReader.GetDateTime(0).Date
                if date <> lastDate then
                    measures <- batch :: measures
                    batch <- makeMeasures date
                    lastDate <- date
                else
                    addMeasuresValues batch

        measures <- batch :: measures
        printfn "\t Loaded %d measures from %d stations." (measuresCount measures) stations.Length
        measures


    let loadFor (pollutant: Pollutant) (stations: IDictionary<string, Station>) (sourcesFiles: string seq) =
        let measuresFrom (path: string) =
            printf "\t Processing file: `%s`..." path

            use stream = File.Open(path, FileMode.Open, FileAccess.Read)
            use excelReader = ExcelReaderFactory.CreateReader(stream)
            extractStations excelReader stations
                |> getMeasures excelReader pollutant
                |> Seq.concat
                |> List.ofSeq

        sourcesFiles
            |> Seq.map measuresFrom
            |> Seq.concat
