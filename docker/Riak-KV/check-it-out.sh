#!/bin/sh

set -e
set -o pipefail

SCRIPT_ROOT=`dirname "$0"`
cd $SCRIPT_ROOT


echo ">> Current Riak KV via Docker-Compose status:"
docker-compose ps


exit 0
