# Riak KV from Basho via Docker

Based on: https://hub.docker.com/r/basho/riak-kv/

* To start it:
  * `_start-it-up.sh`
* To stop it:
  * `_wind-it-down.sh`
* To check its status:
  * `_check-it-out.sh`

----

## Docker's tips

* Full cleanup - removes all containers and images and volumes:
  * `docker rm $(docker ps -a -q)`
  * `docker rmi $(docker images -q)`
  * `docker volume rm $(docker volume ls -q)`
* To end work with Docker via Toolbox's console:
  * `docker-machine stop && exit`
