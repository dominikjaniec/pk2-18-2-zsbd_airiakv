#!/bin/sh

set -e
set -o pipefail

SCRIPT_ROOT=`dirname "$0"`
cd $SCRIPT_ROOT


echo ""
echo ">> Starting Riak KV via Docker-Compose..."
start "Riak KV :: Docker-Compose (output)" \
	docker-compose up

echo ">> To wind it down via Docker, please execute: '$ wind-it-down"
echo ""

MACHINE_URL=`docker-machine ip default`
echo ">> You might now open: http://$MACHINE_URL:8098/admin/"
echo ">> It may take long time, until web-interface becomes available..."


exit 0
