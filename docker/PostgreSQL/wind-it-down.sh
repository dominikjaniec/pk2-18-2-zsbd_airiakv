#!/bin/sh

set -e
set -o pipefail

SCRIPT_ROOT=`dirname "$0"`
cd $SCRIPT_ROOT


echo ">> Winding PostgreSQL via Docker-Compose down..."
docker-compose down


exit 0
