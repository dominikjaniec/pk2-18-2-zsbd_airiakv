#!/bin/sh

set -e
set -o pipefail

SCRIPT_ROOT=`dirname "$0"`
cd $SCRIPT_ROOT


echo ""
echo ">> Starting PostgreSQL server and Admin tool..."
start "PostgreSQL :: Docker-Compose (output)" \
	docker-compose up

echo ">> To wind it down via Docker, please execute: '$ wind-it-down"
echo ""

MACHINE_URL=`docker-machine ip default`
echo ">> You might now open: http://$MACHINE_URL:8080/"
echo ">> Use predefined user: admin@postgre | password: qwe"
echo ">> Add local server at $MACHINE_URL admin | qwe"


exit 0
