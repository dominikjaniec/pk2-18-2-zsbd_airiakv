#!/bin/sh

set -e
set -o pipefail

SCRIPT_ROOT=`dirname "$0"`
cd $SCRIPT_ROOT


echo ""
echo ">> Starting Redis storage via Docker-Compose..."
start "Redis :: Docker-Compose (output)" \
	docker-compose up --scale worker=2

echo ">> To wind it down via Docker, please execute: '$ wind-it-down'"
echo ""

MACHINE_URL=`docker-machine ip default`
echo ">> You might now connect via: http://$MACHINE_URL:6379/"


exit 0
