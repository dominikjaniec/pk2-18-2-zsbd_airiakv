# ZSBD Projekt - Zaawansowane Technologie Baz Danych
## PK FMI Stopień 2, Semestr II (studia niestacjonarne), 2017/2018

----

## RIAK-GIOŚ

### [*Riak KV*](http://docs.basho.com/riak/kv/2.2.3/)
Riak KV is a distributed NoSQL database designed to deliver maximum data availability by distributing data across multiple servers. As long as your Riak KV client can reach one Riak server, it should be able to write data.

### [*GIOŚ*](http://www.gios.gov.pl/pl/)
Główny Inspektorat Ochrony Środowiska

----

Data source: https://powietrze.gios.gov.pl/pjp/archives
